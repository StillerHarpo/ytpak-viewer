{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import           Control.Monad.Catch            ( MonadThrow )
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as T
import           Network.HTTP.Simple            ( getResponseBody
                                                , httpJSON
                                                , parseRequest
                                                )
import           Seralize                       ( )
import           System.Console.ANSI            ( ConsoleLayer(Foreground)
                                                , SGR(SetColor, Reset)
                                                , Color(Red)
                                                , ColorIntensity(Vivid)
                                                , setSGR
                                                )
import           System.Console.Repline         ( CompleterStyle(Word)
                                                , evalRepl
                                                , abort
                                                , HaskelineT
                                                )
import           System.Process                 ( readProcess )
import           Text.Megaparsec                ( parseMaybe )
import           Text.PrettyPrint.Boxes         ( Box
                                                , render
                                                )

import           Lib
import           Command
import           Parser
import           PrettyPrint
import           SearchResults

main :: IO ()
main = repl

type Repl a = HaskelineT (ReaderT (IORef (Maybe SearchResults)) IO) a

search :: (MonadIO m, MonadThrow m) => Text -> m SearchResults
search keyword = do
    request <- parseRequest
        ("https://ytpak.net/search/dosearch?keyword=" <> T.unpack keyword)
    fromJSON keyword . getResponseBody <$> httpJSON request

nextPageSR :: (MonadIO m, MonadThrow m) => SearchResults -> m SearchResults
nextPageSR SearchResults {..} = do
    request <- parseRequest
        (  "https://ytpak.net/search/dosearch?keyword="
        <> T.unpack searchKeyword
        <> "&token="
        <> T.unpack nextPageToken
        )
    fromJSON searchKeyword . getResponseBody <$> httpJSON request

printSR :: (MonadIO m) => SearchResults -> m ()
printSR = liftIO . printStretchedB . prettySearchResults

printStretchedB :: Box -> IO ()
printStretchedB = printStretched . lines . T.pack . render

printStretched :: [Text] -> IO ()
printStretched []             = pure ()
printStretched [l           ] = T.putStrLn l
printStretched (l1 : l2 : ls) = do
    T.putStrLn l1
    setSGR [SetColor Foreground Vivid Red]
    T.putStrLn l2
    setSGR [Reset]
    printStretched ls

play :: SearchResults -> [Text] -> Repl ()
play sr urls = do
    stdOut <- liftIO (readProcess "mpv" (map T.unpack urls) "")
    putStrLn stdOut
    printSR sr

cmd :: String -> Repl ()
cmd input = do
    ref          <- lift ask
    maybeCurPage <- liftIO $ readIORef ref
    case (parseMaybe parseCmd (T.pack input), maybeCurPage) of
        (Just (Select i), Just sr@SearchResults {..}) ->
            case videos !!? (i - 1) of
                Just Video {..} -> play sr [url]
                Nothing         -> putStrLn "Invalid Range"
        (Just (Range i j), Just sr@SearchResults {..}) ->
            case slice (i - 1) (j + 1) videos of
                [] -> putStrLn "Invalid Range"
                vs -> play sr (map url vs)
        (Just (Start i), Just sr@SearchResults {..}) ->
            case drop (i - 1) videos of
                [] -> putStrLn "Invalid Range"
                vs -> play sr (map url vs)
        (Just (Search keyword), _) -> do
            res <- liftIO $ search keyword
            liftIO $ writeIORef ref (Just res)
            printSR res
        (Nothing, _) -> error "parse error shouldn't happen please report bug"
        (_, Nothing) -> putStrLn "First search something"

nextPage :: [String] -> Repl ()
nextPage _ = do
    ref          <- lift ask
    maybeCurPage <- liftIO $ readIORef ref
    case maybeCurPage of
        Just curPage -> do
            res <- liftIO $ nextPageSR curPage
            liftIO $ writeIORef ref (Just res)
            printSR res
        Nothing -> putStrLn "First search something"

help :: [String] -> Repl ()
help _ = liftIO . T.putStrLn $ unlines
    ["Type :h[elp] for help", "Type :n[extPage] for next page"]

opts :: [(String, [String] -> Repl ())]
opts =
    [ ("help"    , help)
    , -- :help
      ("h"       , help)
    , -- :h
      ("n"       , nextPage)
    , ("nextPage", nextPage)
    , ("quit"    , const abort)
    , -- :quit
      ("q"       , const abort) -- :q
    ]

helpText :: String
helpText = "(:h for help)"

ini :: Repl ()
ini = liftIO (putStrLn $ "Welcome to ytpak-viewer" <> helpText)

repl :: IO ()
repl = do
    ref <- newIORef Nothing
    runReaderT
        (evalRepl (pure $ "Search " <> helpText <> " ")
                  cmd
                  opts
                  (Just ':')
                  (Word (\_ -> pure []))
                  ini
        )
        ref
