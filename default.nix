{ sources ? import ./nix/sources.nix
, pkgs ? import ./nix/pinned.nix { inherit sources;}
, compiler ? ""}:

let
  haskellPackages = if compiler == ""
                    then pkgs.haskellPackages
                    else pkgs.haskell.${compiler};

in haskellPackages.developPackage {
  root = ./.;
  modifier = drv:
    pkgs.haskell.lib.overrideCabal drv (attrs: {
      buildTools = (attrs.buildTools or [ ]) ++ [
        haskellPackages.cabal-install
        haskellPackages.haskell-language-server
        haskellPackages.brittany
        pkgs.hlint
      ];
    });
}
