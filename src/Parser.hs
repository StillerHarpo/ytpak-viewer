module Parser where

import           Text.Megaparsec                ( Parsec
                                                , eof
                                                , getInput
                                                , setInput
                                                , try
                                                )
import           Text.Megaparsec.Char           ( char )
import           Text.Megaparsec.Char.Lexer     ( decimal )

import           Command

type Parser = Parsec Void Text

parseCmd :: Parser Cmd
parseCmd =
    try (Select <$> decimal <* eof)
        <|> try (Range <$> decimal <* char '-' <*> decimal <* eof)
        <|> try (Start <$> decimal <* char '-' <* eof)
        <|> Search
        <$> (getInput <* setInput "")
