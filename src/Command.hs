module Command where

data Cmd = Select Int
         | Range Int Int
         | Start Int
         | Search Text
