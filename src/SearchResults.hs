module SearchResults where

import qualified Data.Text                     as T
import           Text.HTML.DOM                  ( parseLT )
import           Text.XML.Cursor                ( Axis
                                                , Cursor
                                                , attribute
                                                , attributeIs
                                                , child
                                                , content
                                                , element
                                                , fromDocument
                                                , parent
                                                )

import           Lib
import qualified Seralize

data Video = Video
    { url      :: Text
    , title    :: Text
    , channel  :: Text
    , duration :: Text
    }
    deriving Show

data SearchResults = SearchResults
    { searchKeyword :: Text
    , nextPageToken :: Text
    , videos        :: [Video]
    }
    deriving Show

fromJSON :: Text -> Seralize.SearchResults -> SearchResults
fromJSON searchKeyword Seralize.SearchResults {..} =
    let
        videos =
            (descendantN 3 >=> maybeToList . parseVideo)
                . fromDocument
                $ parseLT html
    in  SearchResults { .. }

parseVideo :: Cursor -> Maybe Video
parseVideo cur = do
    url <-
        changeProviderURL "youtube.com/"
            <$> ( cur
                & (descendantN 3 >=> element "a" >=> attribute "href")
                & viaNonEmpty head
                )
    title <-
        cur
        & (descendantN 2 >=> classIs "ytv-title" >=> descendantN 2 >=> content)
        & viaNonEmpty head
    channel <-
        cur
        & (   descendantN 2
          >=> classIs "ytv-byline"
          >=> child
          >=> element "a"
          >=> child
          >=> content
          )
        & viaNonEmpty head
    duration <-
        cur
        & (descendantN 3 >=> classIs "duration" >=> child >=> content)
        & viaNonEmpty head
    pure Video { .. }

classIs :: Text -> Axis
classIs = attributeIs "class"

changeProviderURL :: Text -> Text -> Text
changeProviderURL provider url =
    let (http : slash : _ : rest) = splitList '/' $ T.unpack url
    in  T.pack $ concat (http : slash : T.unpack provider : rest)

descendantN :: Int -> Axis
descendantN 0 = (: [])
descendantN n | n < 0 = parent >=> descendantN (n + 1)
              | n > 0 = child >=> descendantN (n - 1)
