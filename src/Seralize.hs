{-# LANGUAGE DeriveGeneric #-}
module Seralize where

import qualified Data.Text.Lazy                as TL
import           Data.Aeson

data SearchResults = SearchResults
    { status        :: Text
    , nextPageToken :: Text
    , html          :: TL.Text
    }
    deriving Generic

instance FromJSON SearchResults where
