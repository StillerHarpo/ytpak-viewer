module Lib where

import Data.Semigroup(Max(..))

slice :: Int -> Int -> [a] -> [a]
slice n m = take (m - n) . drop n

mapi :: (Int -> a -> b) -> [a] -> [b]
mapi = mapiH 1
  where
    mapiH _ _ []       = []
    mapiH i f (x : xs) = f i x : mapiH (i + 1) f xs

map4 :: (a -> b) -> (a,a,a,a) -> (b,b,b,b)
map4 f (a, b, c, d) = (f a, f b, f c, f d)

maximum :: (Foldable t, Ord a, Bounded a ) => t a -> a
maximum = getMax . foldMap Max

-- | Repeadly splits a list by the provided separator and collects the results
splitList :: Eq a => a -> [a] -> [[a]]
splitList _   []   = []
splitList sep list = h : splitList sep t where (h, t) = split (== sep) list

split :: (a -> Bool) -> [a] -> ([a], [a])
split _ [] = ([], [])
split p (x : xs) | p x       = ([x], xs)
                 | otherwise = let (xs1, xs2) = split p xs in (x : xs1, xs2)
