module PrettyPrint where

import           Data.List                      ( unzip4 )
import qualified Data.Text                     as T
import           Text.PrettyPrint.Boxes         ( Box
                                                , hsep
                                                , left
                                                , text
                                                , vcat
                                                )

import           SearchResults
import           Lib

prettyVideo :: Int -> Video -> (Box, Box, Box, Box)
prettyVideo i Video {..} =
    map4 (text . T.unpack) (show i `T.append` ".", title, channel, duration)

vidTextLen :: Video -> Int
vidTextLen Video {..} =
    sum $ map (length . T.unpack) [title, channel, duration]

prettySearchResults :: SearchResults -> Box
prettySearchResults SearchResults {..} =
  if null videos
  then text "No Videos found!"
  else (let (numB, titleB, channelB, durationB) =
               map4 (vcat left) . unzip4 $ mapi prettyVideo videos
        in  hsep
               4
               left
               [ hsep 2 left [numB, titleB]
               , hsep (100 - maximum (map vidTextLen videos))
                      left
                      [channelB, durationB]
               ])
